$( "#btn1" ).click(function() {
  function complete() {
    $( "<div>" ).text( this.id ).appendTo( "#log" );
  }
  $( "#box1" ).fadeOut( 1600, "linear", complete );
  $( "#box2" ).fadeOut( 1600, complete );
});
 
$( "#btn2" ).click(function() {
  $( "div" ).show();
  $( "#log" ).empty();
});